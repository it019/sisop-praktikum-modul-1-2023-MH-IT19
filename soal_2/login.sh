#!/bin/bash

read -p "Masukkan email: " email
read -s -p "Masukkan password: " password

data_olah=$(grep "$email" users.txt)
data=$(grep "$email" users.txt | awk '{print $1}')

if [[ "$email" != "$data" ]]; then
  echo "LOGIN FAILED - Email $email tidak terdaftar"
fi

username=$(echo "$data_olah" | awk '{print $2}')
password_input=$(echo "$data_olah" | awk '{print $3}')

decode_pass=$(echo "$password_input" | base64 --decode)

if [[ "$password" == "$decode_pass" ]]; then
echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN SUCCESS] Welcome, $username" >> auth.log
echo "[LOGIN SUCCESS] - Welcome, $username"

else
  echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN FAILED] - Gagal melakukan login oleh user dengan email $email" >> auth.log
  echo "LOGIN FAILED - Password yang Anda masukkan salah"
fi
