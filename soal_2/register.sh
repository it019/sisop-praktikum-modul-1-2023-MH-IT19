#!/bin/bash

read -p "Masukkan email: " email
read -p "Masukkan username: " username
read -s -p "Masukkan password: " password
echo

if grep -q "$email" users.txt; then
  echo "Gagal melakukan registrasi. Email $email sudah terdaftar. Silakan membuat email baru"
else
  if [[ ${#password} -lt 8 || !("$password" =~ [A-Z]) || !("$password" =~ [a-z]) || !("$password" =~ [0-9]) || !("$password" =~ [^a-zA-Z0-9]) || !("$password" =~ [!@#]) || ("$password" == "$username") ]]; then
    echo "Gagal melakukan registrasi. Password tidak memenuhi persyaratan keamanan."
    echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] Password tidak memenuhi syarat" >> auth.log
  else
    encrypted=$(echo -n "$password" | base64)
    echo "$email $username $encrypted" >> users.txt

    echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER SUCCESS] Pengguna $username berhasil registrasi" >> auth.log

    echo "REGISTER SUCCESS - Pengguna atas nama $username telah berhasil melakukan registrasi."
  fi
fi