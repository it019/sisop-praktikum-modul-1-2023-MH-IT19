# sisop-praktikum-modul-1-2023-MH-IT19


## Soal 1


Pada soal nomor 1 diperlukan file bash dan command untuk mendownload file csv dari google drive, maka digunakan command sebagai berikut.


```sh
wget -O playlist.csv --no-check-certificate -r 'https://drive.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp'
```
Command tersebut dapat mendownload dari google drive dengan menggunakan id url-nya dan menyimpannya dalam output bernama playlist.csv


### Soal 1a


```sh
awk -F ',' '$4 ~ /hip hop/ {print}' playlist.csv | sort -t ',' -k15,15nr | head -n 5
```
Pada soal 1a diharuskan untuk melakukan output 5 lagu teratas berdasarkan popularity dari playlist.csv yang memiliki genre hip hop. Command tersebut berfungsi untuk memanipulasi output dengan hanya mengambil kalimat yang mengandung kata "hip hop" di kolom ke-4 dari playlist.csv. Kemudian output tersebut disortir dengan delimiter ',' dari kolom ke-15(popularity) dengan numerik secara besar ke kecil. Setelah itu digunakan "head" untuk hanya mengambil 5 teratas.


### Soal 1b


```sh
awk -F ',' '$3 ~ /John Mayer/ {print}' playlist.csv | sort -t ',' -k15,15n | head -n 5
```
Pada soal 1b diharuskan untuk melakukan output 5 lagu terbawah berdasarkan popularity dari playlist.csv yang dinyanyikan Joh Mayer. Command tersebut berfungsi untuk memanipulasi output dengan hanya mengambil kalimat yang mengandung kata "John Mayer" di kolom ke-3 dari playlist.csv. Kemudian output tersebut disortir dengan delimiter ',' dari kolom ke-15(popularity) dengan numerik secara kecil ke besar. Setelah itu digunakan "head" untuk hanya mengambil 5 teratas.


### Soal 1c


```sh
awk -F ',' '$5 == 2004 {print}' playlist.csv | sort -t ',' -k15,15nr | head -n 10
```
Pada soal 1c diharuskan untuk melakukan output 5 lagu teratas berdasarkan popularity dari playlist.csv yang dirilis pada tahun 2004. Command tersebut berfungsi untuk memanipulasi output dengan hanya mengambil kalimat "2004" di kolom ke-5 dari playlist.csv. Kemudian output tersebut disortir dengan delimiter ',' dari kolom ke-15(popularity) dengan numerik secara besar ke kecil. Setelah itu digunakan "head" untuk hanya mengambil 5 teratas.


### Soal 1d


```sh
awk -F ',' '/Sri/ {print}' playlist.csv
```
Pada soal 1d diharuskan untuk melakukan output lagu ciptaan Ibu Sri dari playlist.csv. Command tersebut berfungsi untuk memanipulasi output dengan hanya mengambil kalimat yang mengandung "Sri" dari playlist.csv.


### Screenshot hasil run


![ss run soal 1](https://drive.google.com/uc?id=1_IqOGOwslL1INFZ9mYp6w2F0ihGV8I-f)

## Soal 2

### Soal 2a
Secara umum, soal nomor 2 mengharuskan praktikan untuk membuat sebuah registration system menggunakan OS Linux. Terdapat beberapa syarat khusus seperti email yang tidak boleh sama, password yang minimal memiliki 1 simbol unik, dan username tidak boleh sama dengan password.

```sh
read -p "Masukkan email: " email
read -p "Masukkan username: " username
read -s -p "Masukkan password: " password
echo
```

### Soal 2b
Pada waktu pengerjaan soal, terdapat beberapa kendala. Kendala yang saya alami adalah pada saat pembuatan passsword. Saya bingung bagaimana cara menentukan kondisi password yang diwajibkan terdiri dari 1 karakter unik. Namun, saya berhasil mengatasi masalah tersebut dengan menambahkan command berikut:

```sh
!("$password" =~ [!@#])
```
### Soal 2c
Praktikan diminta untuk membuat users.txt untuk menyimpan email, username, dan password 

```touch users.txt```

### Soal 2d
Praktikan diminta untuk memunculkan respons apakah registrasi tersebut berhasil atau gagal

```sh
if grep -q "$email" users.txt; then
  echo "Gagal melakukan registrasi. Email $email sudah terdaftar. Silakan membuat email baru"
else
  if [[ ${#password} -lt 8 || !("$password" =~ [A-Z]) || !("$password" =~ [a-z]) || !("$password" =~ [0-9]) || !("$password" =~ [^a-zA-Z0-9]) || !("$password" =~ [!@#]) || ("$password" == "$username") ]]; then
    echo "Gagal melakukan registrasi. Password tidak memenuhi persyaratan keamanan."
  fi
fi
```

### Soal 2e
Praktikian diminta untuk melakukan uji coba login. Data login didapat dan dicocokkan dari file users.txt

```sh
#!/bin/bash

read -p "Masukkan email: " email
read -s -p "Masukkan password: " password

data_olah=$(grep "$email" users.txt)
data=$(grep "$email" users.txt | awk '{print $1}')

if [[ "$email" != "$data" ]]; then
  echo "LOGIN FAILED - Email $email tidak terdaftar"
fi

username=$(echo "$data_olah" | awk '{print $2}')
password_input=$(echo "$data_olah" | awk '{print $3}')

decode_pass=$(echo "$password_input" | base64 --decode)
```

### Soal 2f
Praktikan diminta untuk mengeluarkan output LOGIN SUCCESS apabila berhasil dan LOGIN FAILED apabila gagal.

```sh
if [[ "$password" == "$decode_pass" ]]; then
echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN SUCCESS] Welcome, $username" >> auth.log
echo "[LOGIN SUCCESS] - Welcome, $username"

else
  echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN FAILED] - Gagal melakukan login oleh user dengan email $email" >> auth.log
  echo "LOGIN FAILED - Password yang Anda masukkan salah"
fi
```

### Soal 2g
Praktikan diminta untuk mengeluarkan output dengan format [TIMESTAMP] [STATUS] setelah selesai registrasi dan output tersebut disimpan ke dalam auth.log

```touch auth.log```

```sh
if grep -q "$email" users.txt; then
  echo "Gagal melakukan registrasi. Email $email sudah terdaftar. Silakan membuat email baru"
else
  if [[ ${#password} -lt 8 || !("$password" =~ [A-Z]) || !("$password" =~ [a-z]) || !("$password" =~ [0-9]) || !("$password" =~ [^a-zA-Z0-9]) || !("$password" =~ [!@#]) || ("$password" == "$username") ]]; then
    echo "Gagal melakukan registrasi. Password tidak memenuhi persyaratan keamanan."
    echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] Password tidak memenuhi syarat" >> auth.log
  else
    encrypted=$(echo -n "$password" | base64)
    echo "$email $username $encrypted" >> users.txt

    echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER SUCCESS] Pengguna $username berhasil registrasi" >> auth.log

    echo "REGISTER SUCCESS - Pengguna atas nama $username telah berhasil melakukan registrasi."
  fi
fi
```

Berikut adalah kode yang saya submit ke asisten penguji sebelum demo dilakukan:

```sh
#!/bin/bash

read -p "Masukkan email: " email
read -p "Masukkan username: " username
read -s -p "Masukkan password: " password
echo

if grep -q "$email" users.txt; then
  echo "Gagal melakukan registrasi. Email $email sudah terdaftar. Silakan membuat email baru"
else
  if [[ ${#password} -lt 8 || !("$password" =~ [A-Z]) || !("$password" =~ [a-z]) || !("$password" =~ [0-9]) || !("$password" =~ [^a-zA-Z0-9]) || !("$password" =~ [!@#]) ]]; then
    echo "Gagal melakukan registrasi. Password tidak memenuhi persyaratan keamanan."
    echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] Password tidak memenuhi syarat" >> auth.log
  else
    encrypted=$(echo -n "$password" | base64)
    echo "$email $username $encrypted" >> users.txt

    echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER SUCCESS] Pengguna $username berhasil registrasi" >> auth.log

    echo "REGISTER SUCCESS - Pengguna atas nama $username telah berhasil melakukan registrasi."
  fi
fi
```
```sh
#!/bin/bash

read -p "Masukkan email: " email
read -s -p "Masukkan password: " password

data_olah=$(grep "$email" users.txt)
data=$(grep "$email" users.txt | awk '{print $1}')

if [[ "$email" != "$data" ]]; then
  echo "LOGIN FAILED - Email $email tidak terdaftar"
fi

username=$(echo "$data_olah" | awk '{print $2}')
password_input=$(echo "$data_olah" | awk '{print $3}')

decode_pass=$(echo "$password_input" | base64 --decode)

if [[ "$password" == "$decode_pass" ]]; then
echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN SUCCESS] Welcome, $username" >> auth.log
echo "[LOGIN SUCCESS] - Welcome, $username"

else
  echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN FAILED] - Gagal melakukan login oleh user dengan email $email" >> auth.log
  echo "LOGIN FAILED - Password yang Anda masukkan salah"
fi
```

Namun, ternyata terdapat masalah lain pada saat demo. Password seharusnya **tidak boleh sama** dengan username, tetapi program tersebut berjalan sebaliknya. Oleh karena itu, saya ubah kode tersebut dan sekarang sudah berjalan normal kembali sesuai yang diinginkan. Berikut adalah kode setelah melakukan demo:

```sh
#!/bin/bash

read -p "Masukkan email: " email
read -p "Masukkan username: " username
read -s -p "Masukkan password: " password
echo

if grep -q "$email" users.txt; then
  echo "Gagal melakukan registrasi. Email $email sudah terdaftar. Silakan membuat email baru"
else
  if [[ ${#password} -lt 8 || !("$password" =~ [A-Z]) || !("$password" =~ [a-z]) || !("$password" =~ [0-9]) || !("$password" =~ [^a-zA-Z0-9]) || !("$password" =~ [!@#]) || ("$password" == "$username") ]]; then
    echo "Gagal melakukan registrasi. Password tidak memenuhi persyaratan keamanan."
    echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER FAILED] Password tidak memenuhi syarat" >> auth.log
  else
    encrypted=$(echo -n "$password" | base64)
    echo "$email $username $encrypted" >> users.txt

    echo "[$(date +"%d/%m/%y %H:%M:%S")] [REGISTER SUCCESS] Pengguna $username berhasil registrasi" >> auth.log

    echo "REGISTER SUCCESS - Pengguna atas nama $username telah berhasil melakukan registrasi."
  fi
fi
```

```sh
#!/bin/bash

read -p "Masukkan email: " email
read -s -p "Masukkan password: " password

data_olah=$(grep "$email" users.txt)
data=$(grep "$email" users.txt | awk '{print $1}')

if [[ "$email" != "$data" ]]; then
  echo "LOGIN FAILED - Email $email tidak terdaftar"
fi

username=$(echo "$data_olah" | awk '{print $2}')
password_input=$(echo "$data_olah" | awk '{print $3}')

decode_pass=$(echo "$password_input" | base64 --decode)

if [[ "$password" == "$decode_pass" ]]; then
echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN SUCCESS] Welcome, $username" >> auth.log
echo "[LOGIN SUCCESS] - Welcome, $username"

else
  echo "[$(date +"%d/%m/%y %H:%M:%S")] [LOGIN FAILED] - Gagal melakukan login oleh user dengan email $email" >> auth.log
  echo "LOGIN FAILED - Password yang Anda masukkan salah"
fi
```

Berikut adalah output dari kode tersebut:

![output](https://drive.google.com/uc?id=1PDMPD95b05UNp9Ab71fRhchq8-qwOIzq)


## Soal 3


Pada bagian awal diperlukan mendownload tautan dari google drive, digunakan command sebagai berikut.


```sh
wget -O genshin.zip --no-check-certificate -r 'https://drive.google.com/uc?export=download&id=1RBuZBtK6q-_I7S_WgeioqIRrUlvRRbq2'
```


Command tersebut dapat mendownload dari google drive dengan menggunakan id url-nya dan menyimpannya dalam output bernama genshin.zip.


### Soal 3a


```sh
unzip genshin.zip


unzip genshin_character.zip -d genshin_character
```


Command tersebut berfungsi untuk melakukan unzip dari file yang telah didownload


```sh
for encoded_filename in genshin_character/*; do
        encoded_basename=$(basename "$encoded_filename")
        decoded_filename=$(echo -n "$encoded_basename" | base64 --decode)
        mv "$encoded_filename" "genshin_character/${decoded_filename}.jpg"


        decoded_basename=$(basename "$decoded_filename")
        character_info=$(grep "$decoded_basename" list_character.csv)


        if [ -n "$character_info" ]; then
                character_name=$(echo "$character_info" | cut -d ',' -f 1)
                character_region=$(echo "$character_info" | cut -d ',' -f 2)
                character_element=$(echo "$character_info" | cut -d ',' -f 3)
                character_weapon=$(echo "$character_info" | cut -d ',' -f 4)
        fi


        mkdir -p "genshin_character/${character_region}"


        new_filename="${character_name} - ${character_region} - ${character_element} - ${character_weapon}"
        clean_filename=$(echo "$new_filename" | tr -d '\015')
        mv "genshin_character/${decoded_filename}.jpg" "genshin_character/${character_region}/${clean_filename}.jpg"
done
```


for loop tersebut berfungsi untuk mengubah nama dari setiap file jpg yang didownload dengan menggunakan base64. Nama file jpg diubah ke "nama - region - element - senjata.jpg". Kemudian setiap file jpg dimasukkan ke folder yang sesuai dengan nama senjata mereka.


### Soal 3b


```sh
for weapon in Catalyst Sword Claymore Bow Polearm; do
        count=$(find genshin_character/ -name "*$weapon.jpg" | wc -l)
        echo "$weapon : $count"
done
```


for loop tersebut berfungsi untuk menhitung jumlah masing-masing senjata. Kemudian masing-masing senjata diprint dengan format "senjata : jumlah".


```sh
rm genshin.zip
rm genshin_character.zip
rm list_character.csv
```


Command tersebut berfungsi untuk menghapus file yang tidak diperlukan.


### Screenshot hasil run genhsin.sh (Soal 3a & 3b)


![ss run soal 3a](https://drive.google.com/uc?id=1QZr7qQbMsavHfuw94dFAtUCCgll-sKDr)
![ss run soal 3b](https://drive.google.com/uc?id=1ExRutxDUP_4KQNpE9vXXyfcROPeLG_uE)


### Soal 3c & 3d


```sh
for region in genshin_character/*; do
        if [ -d "$region" ]; then
                for image in "$region"/*; do
                        image_filename=$(echo "$image" | awk -F ' -' '{print $1}')
                        image_basename=$(echo "$image_filename" | awk -F '/' '{print $3}')
                        steghide extract -sf "$image" -p "" -xf "${image_basename}.txt"


                        decoded_message=$(cat "${image_basename}.txt" | base64 --decode)




                        timestamp=$(date '+%d/%m/%y %H:%M:%S')


                        if [[ "$decoded_message" == *http* ]]; then
                                wget "$decoded_message"
                                echo "[$timestamp] [FOUND] [$image]" >> image.log
                                pkill -f "find_me.sh"
                        else
                                echo "[$timestamp] [NOT FOUND] [$image]" >> image.log
                                rm "${image_basename}.txt"
                        fi
                        sleep 1
                done
        fi
done
```


for loop tersebut berfungsi untuk melakukan ekstrak menggunakan steghide terhadap setiap file jpg yang nantinya isi dari hasil ekstrak akan di-decode menggunakan base64. Kemudian jika ditemukan url, maka url tersebut akan di-download. Kemudian diprint [WAKTU] [FOUND] [PATH] ke dalam image log. Kemudian program dimatikan. Jika tidak ditemukan maka diprint [WAKTU] [NOT FOUND] [PATH] ke image.log. Setelah itu, file hasil ekstrak akan dihapus. Program berjalan setiap 1 detik.


### Screenshot hasil run find_me.sh (Soal 3c, 3d, & 3e)


![soal_3c](/uploads/2db0dc1a7c7b72a12c682c9f5f73e53c/soal_3c.png)
![image.log](https://drive.google.com/uc?id=1kMB-8IEBCORc34QvJyKZm11rkKNsQycN)
![tree](/uploads/a851b1bc112c4b0091d9ae83b7ad6d5d/tree.png)


### Soal Nomor 4
### Soal 4a

### Pembuatan Script minute_log.sh 

```sh
#!/bin/bash

# Log directori untuk menyimpan hasil monitoring
log_dir="/home/imamjk/log2"

# Pengaturan waktu yang digunakan
timestamp=$(date +'%Y%m%d%H%M%S')

# Deklarasi Ram dan swap dengan 'free -m'
ram_info=$(free -m | awk 'NR==2{print $2","$3","$4","$5","$6","$7}')
swap_info=$(free -m | awk 'NR==3{print $2","$3","$4}')

# Target path 'du -sh' yang akan dimonitoring resourcenya
target_path="/home/imamjk/"
dir_size_info=$(du -sh "$target_path" | awk '{print $1}')

# Menyimpan Source monitoring dari target path
echo "$ram_info,$swap_info,$target_path,$dir_size_info" >> "$log_dir/metrics_$timestamp.log"
```


untuk hasil dari script tersebut sebagai berikut:
![minute_sh](https://drive.google.com/uc?id=1-pTn2T3AogeK1YAXgEvXPHxTI7pRzStX) 

dengan hasil tersebut menunjukkan semua hasil monitoring resource dari direktori home/user (imamjk)

### Soal 4b
### Pembuatan cronjob dengan crontab -e untuk mennjalankan script secara otomatis

```sh
  GNU nano 6.2                                         /tmp/crontab.JV1cHT/crontab                                                  


# Menjalankan skrip per menit setiap menit
* * * * * /home/imamjk/SiSop/minute_log.sh

# Menjalankan skrip agregasi per jam setiap jam
0 * * * * /home/imamjk/SiSop/aggregate_minutes_to_hourly_log.sh

```

### Soal 4c
Lalu Pembuatan script agregasi file log ke satuan jam


```sh
#!/bin/bash

# Direktori log
log_dir="/home/imamjk/log2"

# Waktu saat ini untuk agregasi per jam
hourly_timestamp=$(date +'%Y%m%d%H')

# Menggabungkan semua file log per menit dalam satu jam
cat "$log_dir/metrics_$hourly_timestamp"* > "$log_dir/metrics_agg_$hourly_timestamp.log"

# Menghitung nilai minimum, maksimum, dan rata-rata dari metrik RAM dan Disk
min_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | head -n 1)
max_ram=$(awk -F',' 'NR>1{print $2}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -n | tail -n 1)
avg_ram=$(awk -F',' 'NR>1{sum+=$2}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

min_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | head -n 1)
max_disk=$(awk -F',' 'NR>1{print $12}' "$log_dir/metrics_agg_$hourly_timestamp.log" | sort -h | tail -n 1)
avg_disk=$(awk -F',' 'NR>1{sum+=$12}END{print sum/(NR-1)}' "$log_dir/metrics_agg_$hourly_timestamp.log")

# Simpan hasil agregasi dalam satu file
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_cache,mem_available,swap_total,swap_used,swap_free,swap_cache,path,path_size" > "$log_dir/metrics_agg_$hourly_timestamp.log"
echo "minimum,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_ram,$min_disk" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
echo "maximum,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_ram,$max_disk" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
echo "average,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_ram,$avg_disk" >> "$log_dir/metrics_agg_$hourly_timestamp.log"
```

### Untuk hasil dari script agregasi sebagai berikut:
![agregasi](https://drive.google.com/uc?id=1JNtaV3mRxwVF7cjgYqauBddlAR0cYlmG)

### Soal 4d
soal 4d ini berhubungan dengan pembuatan script minute_log.sh

Jadi untuk membuat file log hanya dapat dibuka oleh pemiliknya kita tambahkan command chmod

`chmod 400 "$log_dir/metrics_$timestamp.log"`

jadi untuk hasil akhir dari minute_log.sh akan jadi seperti ini

```sh
#!/bin/bash
timestamp=$(date +'%Y%m%d%H%M%S')
ram_info=$(free -m | awk 'NR==2{print $2","$3","$4","$5","$6","$7}')
swap_info=$(free -m | awk 'NR==3{print $2","$3","$4}')
target_path="/home/imamjk/"
dir_size_info=$(du -sh "$target_path" | awk '{print $1}')
echo "$ram_info,$swap_info,$target_path,$dir_size_info" >> "$log_dir/metrics_$timestamp.log"
chmod 400 "$log_dir/metrics_$timestamp.log"
```