#!/bin/bash

wget -O playlist.csv --no-check-certificate -r 'https://drive.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp'

awk -F ',' '$4 ~ /hip hop/ {print}' playlist.csv | sort -t ',' -k15,15nr | head -n 5

awk -F ',' '$3 ~ /John Mayer/ {print}' playlist.csv | sort -t ',' -k15,15n | head -n 5

awk -F ',' '$5 == 2004 {print}' playlist.csv | sort -t ',' -k15,15nr | head -n 10

awk -F ',' '/Sri/ {print}' playlist.csv
